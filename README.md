# Nuxt.js 在 [21云盒子](https://www.21cloudbox.com/) 的示例

这是 [21云盒子](http://www.21cloudbox.com/) 上创建的 [Nuxtjs](https://zh.nuxtjs.org/) 示例。

这个应用已经部署在 [https://nuxt.21cloudbox.com](https://nuxt.21cloudbox.com)。

## 部署
详情看 [https://www.21cloudbox.com/blog/solutions/how-to-deploy-nuxtjs-project-in-production-server.html](https://www.21cloudbox.com/blog/solutions/how-to-deploy-nuxtjs-project-in-production-server.html)
